const fs = require('fs'),
    request = require('request'),
    cheerio = require("cheerio"),
    async = require('async'),
    iconv = require('iconv-lite'),
    NodeID3 = require('node-id3'),
    config = require('./config.json'),
    _ = require("underscore"),
    util = require('util');




// require('request').debug = true;

//Write log to file
if (config.Write_Log_File) {
    const logFile = fs.createWriteStream('log.txt', { flags: 'a' });
    console.log = function() {
        logFile.write((new Date()).toJSON() + " " + util.format.apply(null, arguments) + '\n');
        process.stdout.write((new Date()).toJSON() + " " + util.format.apply(null, arguments) + '\n');
    }
    console.error = console.log;
}


//Generate FileName
const FormatNumberLength = (num, length) => {
    let r = "" + num;
    while (r.length < length) {
        r = "0" + r;
    }
    return r;
}

const SaveTaskStatus = (taskid, status) => {
    fs.writeFileSync("./" + config.Tasks[taskid].Dir_Name + '/status.json', JSON.stringify(status, null, 4));
}

let task_status = null;
let current_task_id = 0;

const mainProcess = callback => {
    const current_task = config.Tasks[current_task_id];

    const path = "./" + current_task.Dir_Name;
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path, 0744);
    }

    //get the status saving in the status.json
    task_status = {
        Files: {},
        Status: "Waiting"
    };

    if (fs.existsSync(path + '/status.json')) {
        task_status = require(path + '/status.json');
    }


    //check the status with the exsit files
    let existfiles = fs.readdirSync(path);

    let downfiles = _.filter(existfiles, n => n.endsWith('.mp3') && n.length == 8).sort();
    //get the max of exist file and the file in the state.json
    let check_to = Math.max(downfiles.length > 0 ? parseInt(downfiles[downfiles.length - 1].substr(0, 4)) : 0,
        ..._.keys(task_status.Files).map(n => parseInt(n)));

    for (let i = current_task.Start_From; i <= check_to; i++) {

        if (fs.existsSync(path + "/" + FormatNumberLength(i, 4) + '.mp3')) {
            const stat = fs.statSync(path + "/" + FormatNumberLength(i, 4) + '.mp3');
            if (_.isUndefined(task_status.Files[i])) {
                task_status.Files[i] = {
                    Status: "Checking",
                    TotalSize: -1,
                    TrueSize: 0
                };
            }
            task_status.Files[i].TrueSize = stat.size;
            if (task_status.Files[i].Status == "Finished" && (task_status.Files[i].TotalSize - task_status.Files[i].TrueSize) > 100) {
                task_status.Files[i].Status = "Waiting";
            }
        } else {
            task_status.Files[i] = {
                Status: "Waiting",
                TotalSize: -1,
                TrueSize: 0
            };
        }
    }

    SaveTaskStatus(current_task_id, task_status);

    task_status.Status = "Downloading";
    async.waterfall([
        //get the list of the pages.
        cb => {
            try {
                request.get(current_task.List_URL).on('response', (res) => {
                    let rawData = '';
                    res.on('data', chunk => rawData += chunk);
                    res.on('error', err => cb(err));
                    res.on('end', () => {
                        const $ = cheerio.load(rawData);
                        let ret = $(".bfdz a").map((i, n) => {
                            return {
                                id: i + 1,
                                url: n.attribs.href
                            };
                        });

                        cb(null, _.filter(ret.slice(current_task.Start_From - 1), n => !task_status.Files[n.id] || task_status.Files[n.id].Status != "Finished"));
                    });
                });
            } catch (e) {
                cb(e);
            }
        },
        //get the url of the mp3s
        (urls, cb) => {
            async.eachLimit(urls, config.Threads_Count, (url_info, cb_0) => {
                    async.waterfall([
                        //get iframe url
                        cb_1 => {
                            try {
                                request.get("https://www.psttt.com" + url_info.url)
                                    .on('response', (res) => {
                                        let rawData = '';
                                        res.on('data', chunk => rawData += chunk);
                                        res.on('end', () => {
                                            const $ = cheerio.load(rawData);
                                            cb_1(null, $("iframe")[0].attribs.src);
                                        });
                                    }).on('error', err => {
                                        cb_1(err);
                                    });
                            } catch (e) {
                                cb_1(e);
                            }
                        },
                        //get mp3 file url
                        (url, cb_2) => {
                            try {
                                request({
                                    url: "https://www.psttt.com" + url,
                                    encoding: null
                                }, (err, res, body) => {
                                    if (!err && res.statusCode == 200) {
                                        let html = iconv.decode(body, 'gb2312').toString();

                                        let p1 = html.indexOf("mp3:'");
                                        let p2 = html.indexOf("\n", p1 + 4);
                                        let url0 = html.substring(p1 + 4, p2 - 1);
                                        // console.log(url0);

                                        p1 = url0.indexOf("+");
                                        p2 = url0.lastIndexOf("+");
                                        let var_name = url0.substring(p1 + 1, p2);
                                        // console.log(var_name);

                                        p1 = html.indexOf("\n" + var_name + " = '");
                                        p2 = html.indexOf("';", p1 + var_name.length + 4);
                                        let var_value = html.substring(p1 + 4 + var_name.length, p2 + 1);
                                        // console.log(var_value);
                                        if (var_value.indexOf(var_name) > -1) {
                                            p1 = html.indexOf("\n" + "m" + var_name + " = '");
                                            p2 = html.indexOf("';", p1 + var_name.length + 4 + 1);
                                            let mvar_value = html.substring(p1 + 4 + var_name.length, p2 + 1);
                                            var_value = var_value.replace('m' + var_name, mvar_value);
                                            // console.log(var_value);
                                        }

                                        url0 = url0.replace(var_name, var_value);
                                        // console.log(url0);
                                        url0 = eval(url0);
                                        // console.log(url0);
                                        cb_2(null, url0);
                                    }
                                });
                            } catch (e) {
                                cb_2(e);
                            }
                        },
                        //download mp3 from the url                    
                        (url, cb_3) => {

                            try {
                                url = encodeURI(url);

                                var total_bytes = 0;

                                if (_.isUndefined(task_status.Files[url_info.id])) {
                                    task_status.Files[url_info.id] = {
                                        Status: "Downloading",
                                        TotalSize: -1,
                                        TrueSize: 0
                                    };
                                }

                                if (task_status.Files[url_info.id].Status == "Checking") {
                                    request({
                                        url: url,
                                        method: "GET"
                                    }).on('response', res => {
                                        total_bytes = parseInt(res.headers['content-length']);
                                        if (total_bytes == task_status.Files[url_info.id].TrueSize) {
                                            console.log("File " + FormatNumberLength(url_info.id, 4) + '.mp3 downloaded.');
                                            task_status.Files[url_info.id].TotalSize = total_bytes;
                                            task_status.Files[url_info.id].Status = "Finished";
                                            SaveTaskStatus(current_task_id, task_status);
                                            cb_3(null);
                                        } else {
                                            var received_bytes = 0;


                                            if (fs.existsSync(path + "/" + FormatNumberLength(url_info.id, 4) + '.mp3')) {
                                                console.log("Delete exist " + FormatNumberLength(url_info.id, 4) + '.mp3');
                                                fs.unlinkSync(path + "/" + FormatNumberLength(url_info.id, 4) + '.mp3');
                                                task_status.Files[url_info.id].TrueSize = 0;
                                            }

                                            let file_stream;
                                            if (task_status.Files[url_info.id].TrueSize > 0 && false) {
                                                file_stream = fs.createWriteStream(path + "/" + FormatNumberLength(url_info.id, 4) + '.mp3', { flags: 'a' });
                                                console.log("Continue download File " + FormatNumberLength(url_info.id, 4) + '.mp3');
                                            } else {
                                                file_stream = fs.createWriteStream(path + "/" + FormatNumberLength(url_info.id, 4) + '.mp3');
                                                console.log("Start download File " + FormatNumberLength(url_info.id, 4) + '.mp3');
                                            }

                                            let c = 0;

                                            request({
                                                    url: url,
                                                    // headers: {
                                                    //     "Range": 'bytes=' + task_status.Files[url_info.id].TrueSize - 1
                                                    // },
                                                    method: "GET"
                                                }).on('response', res => {
                                                    total_bytes = parseInt(res.headers['content-length']);
                                                    task_status.Files[url_info.id].TotalSize = total_bytes;
                                                    task_status.Files[url_info.id].Status = "Downloading";
                                                    SaveTaskStatus(current_task_id, task_status);
                                                })
                                                .on('data', chunk => {
                                                    received_bytes += chunk.length;
                                                    task_status.Files[url_info.id].TrueSize = received_bytes;
                                                    if (c++ % 50 == 0) {
                                                        console.info((new Date()).toJSON() + "  File " + FormatNumberLength(url_info.id, 4) + '.mp3 :' +
                                                            ((received_bytes / total_bytes * 100).toFixed(4)) + "%");

                                                        SaveTaskStatus(current_task_id, task_status);
                                                    }
                                                })
                                                .on('error', err => {
                                                    if (file_stream.status)
                                                        file_stream.close();
                                                    cb_3(err);
                                                })
                                                .on('end', () => {
                                                    NodeID3.write({
                                                        title: config.Tasks[current_task_id].Name + FormatNumberLength(url_info.id, 4),
                                                        album: config.Tasks[current_task_id].Name,
                                                        artist: " ",
                                                        TRCK: url_info.id
                                                    }, path + "/" + FormatNumberLength(url_info.id, 4) + '.mp3');
                                                    console.log("File " + FormatNumberLength(url_info.id, 4) + '.mp3 downloaded.');
                                                    task_status.Files[url_info.id].TrueSize = task_status.Files[url_info.id].TotalSize;
                                                    task_status.Files[url_info.id].Status = "Finished";
                                                    SaveTaskStatus(current_task_id, task_status);
                                                    cb_3(null);
                                                })
                                                .pipe(file_stream);
                                        }

                                    });
                                } else {

                                    var received_bytes = 0;


                                    if (fs.existsSync(path + "/" + FormatNumberLength(url_info.id, 4) + '.mp3')) {
                                        console.log("Delete exist " + FormatNumberLength(url_info.id, 4) + '.mp3');
                                        fs.unlinkSync(path + "/" + FormatNumberLength(url_info.id, 4) + '.mp3');
                                        task_status.Files[url_info.id].TrueSize = 0;
                                    }

                                    let file_stream;
                                    if (task_status.Files[url_info.id].TrueSize > 0 && false) {
                                        file_stream = fs.createWriteStream(path + "/" + FormatNumberLength(url_info.id, 4) + '.mp3', { flags: 'a' });
                                        console.log("Continue download File " + FormatNumberLength(url_info.id, 4) + '.mp3');
                                    } else {
                                        file_stream = fs.createWriteStream(path + "/" + FormatNumberLength(url_info.id, 4) + '.mp3');
                                        console.log("Start download File " + FormatNumberLength(url_info.id, 4) + '.mp3');
                                    }

                                    let c = 0;

                                    request({
                                            url: url,
                                            // headers: {
                                            //     "Range": 'bytes=' + task_status.Files[url_info.id].TrueSize - 1
                                            // },
                                            method: "GET"
                                        }).on('response', res => {
                                            total_bytes = parseInt(res.headers['content-length']);
                                            task_status.Files[url_info.id].TotalSize = total_bytes;
                                            task_status.Files[url_info.id].Status = "Downloading";
                                            SaveTaskStatus(current_task_id, task_status);
                                        })
                                        .on('data', chunk => {
                                            received_bytes += chunk.length;
                                            task_status.Files[url_info.id].TrueSize = received_bytes;
                                            if (c++ % 50 == 0) {
                                                console.info((new Date()).toJSON() + "  File " + FormatNumberLength(url_info.id, 4) + '.mp3 :' +
                                                    ((received_bytes / total_bytes * 100).toFixed(4)) + "%");

                                                SaveTaskStatus(current_task_id, task_status);
                                            }
                                        })
                                        .on('error', err => {
                                            if (file_stream.status)
                                                file_stream.close();
                                            cb_3(err);
                                        })
                                        .on('end', () => {
                                            NodeID3.write({
                                                title: config.Tasks[current_task_id].Name + FormatNumberLength(url_info.id, 4),
                                                album: config.Tasks[current_task_id].Name,
                                                artist: " ",
                                                TRCK: url_info.id
                                            }, path + "/" + FormatNumberLength(url_info.id, 4) + '.mp3');
                                            console.log("File " + FormatNumberLength(url_info.id, 4) + '.mp3 downloaded.');
                                            task_status.Files[url_info.id].TrueSize = task_status.Files[url_info.id].TotalSize;
                                            task_status.Files[url_info.id].Status = "Finished";
                                            SaveTaskStatus(current_task_id, task_status);
                                            cb_3(null);
                                        })
                                        .pipe(file_stream);
                                }
                            } catch (err) {
                                cb_3(err);
                            }
                        }
                    ], err => {
                        cb_0(err);
                    });
                },
                err => {
                    cb(err);
                });
        }
    ], (err, result) => {
        if (err == null) {
            task_status.Status = "Finished";
            config.Tasks[current_task_id].Status = "Finished";
            SaveTaskStatus(current_task_id, task_status);
        } else {
            console.error(err);
            console.log("Try again .............................................................");
        }
        callback();
    });
}

const Start = () => {

    console.log("Start Process ......");
    async.doUntil(mainProcess,
        () => {
            for (let i = 0; i < config.Tasks.length; i++) {
                if (config.Tasks[i].Status != "Finished") {
                    current_task_id = i;
                    return false
                }
            }
            return true;
        },
        err => {
            debugger;
            if (err) {
                console.error("Final error:");
                console.error(err);
            } else {
                console.log('Finished.');
            }
        });
}

exports.start = Start;